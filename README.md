# gen-runner

## About this?

`docker-compose`を用いて`gitlab-runner`の設定と起動を行うためのテンプレートです。

## Require

- Docker

## How to use

- 環境変数`GITLAB_TOKEN`にregistration tokenを記述します。リポジトリの`Settings→CI/CD→Runners`で確認できます。![img001](images/img001.png)

```bash
export GITLAB_TOKEN={registration tokenの値}
```

- WSL2を含むLinux環境の場合は `linux`ディレクトリ、Macの場合は`mac`ディレクトリに移動します。
- 以下のコマンドでgitlab-runnerのdockerコンテナを起動します。

```bash
docker-compose up -d
```

- 以下のコマンドで立ち上げたgitlab-runnerをGitlabリポジトリに連携します。

```bash
docker-compose exec runner gitlab-runner register
```

- 以下のような出力が得られれば成功です。ブラウザをリロードし、リポジトリに反映されていることも確認します。

```txt
Registering runner... succeeded
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

![img002](images/img002.png)

- 登録したgitlab-runnerの編集ボタン(鉛筆マーク)を押下します。
- `Run untagged jobs Indicates whether this runner can pick jobs without tags`を有効にします。

![img003](images/img003.png)
